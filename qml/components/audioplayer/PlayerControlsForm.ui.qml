import QtQuick 2.6
import Sailfish.Silica 1.0

Row {
    property bool isPlaying: false
    property bool isStared: false
    property alias buttonStar: buttonStar
    property alias buttonPrevious: buttonPrevious
    property alias buttonPlay: buttonPlay
    property alias buttonNext: buttonNext


    id: controls
    property real itemWidth: width / 4

    IconButton {
        id: buttonStar
        width: itemWidth
        anchors.verticalCenter: parent.verticalCenter
        icon.source: isStared ? "image://theme/icon-m-favorite-selected" : "image://theme/icon-m-favorite"
    }

    IconButton {
        id: buttonPrevious
        width: itemWidth
        anchors.verticalCenter: parent.verticalCenter
        icon.source: "image://theme/icon-m-previous-song"
    }

    IconButton {
        id: buttonPlay
        width: itemWidth
        anchors.verticalCenter: parent.verticalCenter
        icon.source: isPlaying ? "image://theme/icon-m-pause" : "image://theme/icon-m-play"
    }

    IconButton {
        id: buttonNext
        width: itemWidth
        anchors.verticalCenter: parent.verticalCenter
        icon.source: "image://theme/icon-m-next-song"
    }
}

