<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>AboutPageForm.ui</name>
    <message>
        <source>About Received</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Report an Issue</source>
        <translation>Сообщить об ошибке</translation>
    </message>
    <message>
        <source>Source code</source>
        <translation>Исходный код</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>Версия: </translation>
    </message>
    <message>
        <source>Translations</source>
        <translation>Переводы</translation>
    </message>
</context>
<context>
    <name>ApiLanguageListModel</name>
    <message>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <source>German</source>
        <translation>Немецкий</translation>
    </message>
    <message>
        <source>Austrian</source>
        <translation>Немецкий (Австрия)</translation>
    </message>
    <message>
        <source>French</source>
        <translation>Французский</translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation>Португальский</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation>Испанский</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation>Шведский</translation>
    </message>
    <message>
        <source>Danish</source>
        <translation>Датский</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation>Итальянский</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation>Польский</translation>
    </message>
</context>
<context>
    <name>BrowseByCategoryPageForm.ui</name>
    <message>
        <source>Browse by</source>
        <translation>Просматривать по</translation>
    </message>
</context>
<context>
    <name>BrowsePage</name>
    <message>
        <source>Local</source>
        <translation>Местные</translation>
    </message>
    <message>
        <source>Top 100</source>
        <translation>Топ 100</translation>
    </message>
    <message>
        <source>Recommended</source>
        <translation>Рекомендуемые</translation>
    </message>
    <message>
        <source>Genre</source>
        <translation>По жанрам</translation>
    </message>
    <message>
        <source>Topic</source>
        <translation>По темам</translation>
    </message>
    <message>
        <source>Country</source>
        <translation>По странам</translation>
    </message>
    <message>
        <source>City</source>
        <translation>По городам</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>По языку</translation>
    </message>
</context>
<context>
    <name>BrowsePageForm.ui</name>
    <message>
        <source>Browse</source>
        <translation>Список станций</translation>
    </message>
</context>
<context>
    <name>DockedAudioPlayerForm.ui</name>
    <message>
        <source>Sleep timer</source>
        <translation>Таймер сна</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <source>Set</source>
        <translation>Применить</translation>
    </message>
</context>
<context>
    <name>FavoritePageForm.ui</name>
    <message>
        <source>http://mystation.com/stream.mp3</source>
        <translation>http://моерадио.рф/stream.mp3</translation>
    </message>
    <message>
        <source>URL</source>
        <translation>URL</translation>
    </message>
    <message>
        <source>My Station</source>
        <translation>Моя станция</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Название</translation>
    </message>
    <message>
        <source>http://mystation.com/logo.jpg</source>
        <translation>http://моерадио.рф/logo.jpg</translation>
    </message>
    <message>
        <source>Logo</source>
        <translation>Эмблема</translation>
    </message>
    <message>
        <source>Sweden</source>
        <translation>Россия</translation>
    </message>
    <message>
        <source>Country</source>
        <translation>Страна</translation>
    </message>
    <message>
        <source>Pop</source>
        <translation>Поп</translation>
    </message>
    <message>
        <source>Genre</source>
        <translation>Жанр</translation>
    </message>
</context>
<context>
    <name>FavoritesListContextMenu</name>
    <message>
        <source>Remove from favorite</source>
        <translation>Удалить из списка избранных</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Удаление</translation>
    </message>
    <message>
        <source>Edit favorite</source>
        <translation>Редактировать список избранных</translation>
    </message>
</context>
<context>
    <name>FavoritesPageForm.ui</name>
    <message>
        <source>Favorites</source>
        <translation>Избранные</translation>
    </message>
    <message>
        <source>From </source>
        <translation>Из </translation>
    </message>
    <message>
        <source>Add Custom</source>
        <translation>Добавить свою станцию</translation>
    </message>
</context>
<context>
    <name>NavigationMenuForm.ui</name>
    <message>
        <source>Settings</source>
        <translation>Настройка</translation>
    </message>
    <message>
        <source>Show player</source>
        <translation>Показывать кнопки проигрывателя</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation>Список станций</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Поиск станций</translation>
    </message>
</context>
<context>
    <name>PlayerLayoutListModel</name>
    <message>
        <source>Original player</source>
        <translation>стандартный</translation>
    </message>
    <message>
        <source>Small player</source>
        <translation>уменьшенный</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Droping DB</source>
        <translation>Стирание БД</translation>
    </message>
</context>
<context>
    <name>SettingsPageForm.ui</name>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Настройка</translation>
    </message>
    <message>
        <source>Advanced Options</source>
        <translation>Дополнительные параметры</translation>
    </message>
    <message>
        <source>Reset DB</source>
        <translation>Сброс БД</translation>
    </message>
    <message>
        <source>API Language:</source>
        <translation>Язык API</translation>
    </message>
    <message>
        <source>Player layout</source>
        <translation>Вид проигрывателя</translation>
    </message>
    <message>
        <source>API Options</source>
        <translation>Настройка API</translation>
    </message>
    <message>
        <source>Basic Options</source>
        <translation>Основные параметры</translation>
    </message>
    <message>
        <source>Sets what style to use for player controls</source>
        <translation>Задает стиль отображения кнопок управления прослушиванием</translation>
    </message>
    <message>
        <source>Removes everything in the database and gives you a clean start. &lt;i&gt;&lt;b&gt;Used with caution&lt;/b&gt;&lt;/i&gt;</source>
        <translation>&lt;i&gt;&lt;b&gt;Осторожно!&lt;/b&gt;&lt;/i&gt; Эта функция полностью стирает базу данных приложения и позволяет начать с чистого листа.</translation>
    </message>
    <message>
        <source>Sets the endpoint to be used for API calls e.g. radio.net for English and radio.de for German. Also effects Top 100 and translations back from the api</source>
        <translation>Определяет сервер, куда направляются API-вызовы, например radio.net для английского и radio.de для немецкого. Также влияет на содержание Топ 100 и язык сообщений от сервера.</translation>
    </message>
</context>
<context>
    <name>StationsListContextMenu</name>
    <message>
        <source>Remove from favorite</source>
        <translation>Удалить из списка избранных</translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation>Удаление</translation>
    </message>
    <message>
        <source>Add to favorite</source>
        <translation>Добавить в список избранных</translation>
    </message>
</context>
<context>
    <name>StationsPageForm.ui</name>
    <message>
        <source>From</source>
        <translation>Из </translation>
    </message>
</context>
<context>
    <name>TranslationCreditsListModel</name>
    <message>
        <source>French</source>
        <translation>Французский</translation>
    </message>
    <message>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation>Испанский</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation>Шведский</translation>
    </message>
</context>
<context>
    <name>TranslationCreditsPageForm.ui</name>
    <message>
        <source>Translations</source>
        <translation>Переводы</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <source>Top 100</source>
        <translation>Топ 100</translation>
    </message>
    <message>
        <source>Search station</source>
        <translation>Поиск станций</translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation>Избранные</translation>
    </message>
    <message>
        <source>Recommended</source>
        <translation>Рекомендуемые</translation>
    </message>
    <message>
        <source>Local</source>
        <translation>Местные</translation>
    </message>
</context>
</TS>
