import QtQuick 2.6
import Sailfish.Silica 1.0
import it.kempe.AudioPlayer 1.0

import "../components/js/Favorites.js" as FavoritesUtils
import "./layouts"

CoverBackground {
    CoverDesign {
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: coverActionArea.top
        anchors.left: parent.left
        title: AudioPlayer.title ? AudioPlayer.station + "\n\n" + AudioPlayer.title
                                 : AudioPlayer.station
        thumbnail: setThumbnail()
    }

    function setThumbnail() {
        if (app.stationData && app.stationData.stationLogo) {
            return app.stationData.stationLogo
        }
        return "/usr/share/icons/hicolor/256x256/apps/harbour-received.png";
    }

    function playFavorit() {
        if(!app.stationData)
            player.playNext()
        else
            AudioPlayer.togglePlayback()
    }

    CoverActionList {
        id: singleFavoriteCoverAction
        enabled: FavoritesUtils.hasOneFavorites() || (!FavoritesUtils.hasFavorites() && AudioPlayer.isPlayable)

        CoverAction {
            iconSource: AudioPlayer.isPlaying ? "image://theme/icon-cover-pause" : "image://theme/icon-cover-play"
            onTriggered: {
                playFavorit()
            }
        }
    }

    CoverActionList {
        id: multipleFavoriteCoverAction
        enabled: FavoritesUtils.hasMultipleFavorites()

        CoverAction {
            iconSource: AudioPlayer.isPlaying ? "image://theme/icon-cover-pause" : "image://theme/icon-cover-play"
            onTriggered: {
                playFavorit()
            }
        }

        CoverAction {
            iconSource: "image://theme/icon-cover-next-song"
            onTriggered: {
                player.playNext()
            }
        }
    }
}


