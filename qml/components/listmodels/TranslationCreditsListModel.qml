import QtQuick 2.0

ListModel {
     id: model

     ListElement {
         language: qsTr("French")
         translators: [
            ListElement { name: "lutinotmalin" }
         ]
     }

     ListElement {
         language: qsTr("Russian")
         translators: [
            ListElement { name: "Вячеслав Диконов" }
         ]
     }

     ListElement {
         language: qsTr("Spanish")
         translators: [
            ListElement { name: "Carmen F. B" }
         ]
     }

     ListElement {
         language: qsTr("Swedish")
         translators: [
            ListElement { name: "Kempe" }
         ]
     }
 }
