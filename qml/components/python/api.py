# -*- coding: utf-8 -*-

import pyotherside
import json
from urllib.parse import urlencode
from urllib.request import urlopen, Request, HTTPError, URLError

USER_AGENT = "XBMC Addon Radio"

genresURL = "info/v2/search/getgenres"
topicsURL = "info/v2/search/gettopics"
countriesURL = "info/v2/search/getcountries"
citiesURL = "info/v2/search/getcities"
languagesURL = "info/v2/search/getlanguages"

recomendedStationsURL = "info/v2/search/editorstips"
topStationsURL = "info/v2/search/topstations"
localStationsURL = "info/v2/search/localstations"

stationsByGenreURL = "info/v2/search/stationsbygenre"
stationsByTopicURL = "info/v2/search/stationsbytopic"
stationsByCountryURL = "info/v2/search/stationsbycountry"
stationsByCityURL = "info/v2/search/stationsbycity"
stationsByLanguageURL = "info/v2/search/stationsbylanguage"
stationsByQueryURL = "info/v2/search/stations"

stationByIdURL = "info/v2/search/station"

SORT_TYPES = {
    'popular': 'RANK',
    'az': 'STATION_NAME'
}

RESULTS_PER_PAGE = 100

class RadioApiError(Exception):
    def __init__(self, message):
        self.message = message

class Radio_API:
    def __init__(self):
        self.apiBaseUrl = "http://api.rad.io"
        self.sortType = SORT_TYPES['popular']
        pass

    def setApiBaseUrl(self, apiUrl):
        self.apiBaseUrl = apiUrl
        self.log("setting apiBaseUrl: {}".format(apiUrl))
        return True

    def getGenres(self):
        return self.__api_call(genresURL)

    def getTopics(self):
        return self.__api_call(topicsURL)

    def getCountries(self):
        return self.__api_call(countriesURL)

    def getCities(self, country=None):
        params = {'country': country} if country else None
        return self.__api_call(citiesURL, params)

    def getLanguages(self):
        return self.__api_call(languagesURL)

    def getRecomendedStations(self):
        response = self.__api_call(recomendedStationsURL)
        if not response:
            raise ValueError("Invalid data returned")
        return self.__normalizeStationData(response)

    def getTopStations(self, sizeperpage=RESULTS_PER_PAGE, pageindex=1):
        # TODO refactor to support pagesize and index
        params = {'sizeperpage': sizeperpage, 'pageindex': pageindex}
        response = self.__api_call(topStationsURL, params)

        if not response or not response.get('categories'):
            raise ValueError("Invalid data returned")
        return self.__normalizeStationData(response.get('categories')[0].get('matches'))

    def getLocalStations(self, sizeperpage=RESULTS_PER_PAGE, pageindex=1):
        # TODO refactor to support pagesize and index
        params = {'sizeperpage': sizeperpage, 'pageindex': pageindex}
        response = self.__api_call(localStationsURL, params)

        if not response or not response.get('categories'):
            raise ValueError("Invalid data returned")
        return self.__normalizeStationData(response.get('categories')[0].get('matches'))

    def getStationsByGenre(self, genre, sorttype=SORT_TYPES['popular'], sizeperpage=RESULTS_PER_PAGE, pageindex=1):
        # TODO refactor to support pagesize, index and sorttype
        params = {'genre': genre, 'sorttype': sorttype, 'sizeperpage': sizeperpage, 'pageindex': pageindex}
        response = self.__api_call(stationsByGenreURL, params)

        if not response or not response.get('categories'):
            raise ValueError("Invalid data returned")
        return self.__normalizeStationData(response.get('categories')[0].get('matches'))

    def getStationsByTopic(self, topic, sorttype=SORT_TYPES['popular'], sizeperpage=RESULTS_PER_PAGE, pageindex=1):
        # TODO refactor to support pagesize, index and sorttype
        params = {'topic': topic, 'sorttype': sorttype, 'sizeperpage': sizeperpage, 'pageindex': pageindex}
        response = self.__api_call(stationsByTopicURL, params)

        if not response or not response.get('categories'):
            raise ValueError("Invalid data returned")
        return self.__normalizeStationData(response.get('categories')[0].get('matches'))

    def getStationsByCountry(self, country, sorttype=SORT_TYPES['popular'], sizeperpage=RESULTS_PER_PAGE, pageindex=1):
        # TODO refactor to support pagesize, index and sorttype
        params = {'country': country, 'sorttype': sorttype, 'sizeperpage': sizeperpage, 'pageindex': pageindex}
        response = self.__api_call(stationsByCountryURL, params)

        if not response or not response.get('categories'):
            raise ValueError("Invalid data returned")
        return self.__normalizeStationData(response.get('categories')[0].get('matches'))

    def getStationsByCity(self, city, sorttype=SORT_TYPES['popular'], sizeperpage=RESULTS_PER_PAGE, pageindex=1):
        # TODO refactor to support pagesize, index and sorttype
        params = {'city': city, 'sorttype': sorttype, 'sizeperpage': sizeperpage, 'pageindex': pageindex}
        response = self.__api_call(stationsByCityURL, params)

        if not response or not response.get('categories'):
            raise ValueError("Invalid data returned")
        return self.__normalizeStationData(response.get('categories')[0].get('matches'))

    def getStationsByLanguage(self, language, sorttype=SORT_TYPES['popular'], sizeperpage=RESULTS_PER_PAGE, pageindex=1):
        # TODO refactor to support pagesize, index and sorttype
        params = {'language': language, 'sorttype': sorttype, 'sizeperpage': sizeperpage, 'pageindex': pageindex}
        response = self.__api_call(stationsByLanguageURL, params)

        if not response or not response.get('categories'):
            raise ValueError("Invalid data returned")
        return self.__normalizeStationData(response.get('categories')[0].get('matches'))

    def getStationsByQuery(self, query, sorttype=SORT_TYPES['popular'], sizeperpage=RESULTS_PER_PAGE, pageindex=1):
        self.log("Searching for: {0}, with pageSize: {1} and page: {2}".format(query, sizeperpage, pageindex))
        params = {'query': query, 'sorttype': sorttype, 'sizeperpage': sizeperpage, 'pageindex': pageindex}
        response = self.__api_call(stationsByQueryURL, params)

        if not response or not response.get('categories'):
            raise ValueError("Invalid data returned")
        return self.__normalizeStationData(response.get('categories')[0].get('matches'))

    def getStationById(self, id):
        self.log("Getting station: {}".format(id))
        params = {'station': str(id)}
        response = self.__api_call(stationByIdURL, params)
        station = self.__addStreamUrlToStation(response)
        self.debug("response: {}".format(station))
        return self.__normalizeStationData((station, ))[0]

    def __api_call(self, path, param=None):
        self.log('__api_call started with path=%s, param=%s'
                 % (path, param))
        url = '%s/%s' % (self.apiBaseUrl, path)
        if param:
            url += '?%s' % urlencode(param)

        try:
            response = self.__urlopen(url)
            return json.loads(response)
        except RadioApiError as error:
            self.notify_error(error.message)
        except:
            self.notify_error("Unknown error")

    def __urlopen(self, url):
        self.debug('__urlopen opening url=%s' % url)
        req = Request(url)
        req.add_header('User-Agent', USER_AGENT)

        try:
            return urlopen(req).read()
        except HTTPError as error:
            self.log('__urlopen HTTPError: {0}'.format(error.reason))
            raise RadioApiError('HTTPError: {0} - {1}'.format(error.code, error.reason))
        except URLError as error:
            self.log('__urlopen URLError: {0}'.format(error))
            raise RadioApiError('URLError: {0}'.format(error.reason))

    @staticmethod
    def __normalizeStationData(stations):
            normalizedStations = []
            for station in stations:
                pyotherside.send('debug', "Normalizing station data for: {0}".format(station))

                thumbnail = (
                    station.get('logo300x300') or
                    station.get('logo175x175') or
                    station.get('logo100x100') or
                    station.get('logo44x44')
                )

                try:
                    genre = [g['value'] for g in station.get('genres')]
                except:
                    genre = [g for g in station.get('genres')]

                description = Radio_API.__getStationPropertyValue(station.get('description'))
                name = Radio_API.__getStationPropertyValue(station.get('name'))
                country = Radio_API.__getStationPropertyValue(station.get('country'))

                normalizedStation = {
                    'id': station['id'],
                    'name': name,
                    'thumbnail': thumbnail,
                    'rating': station.get('rank', ''),
                    'genres': ', '.join(genre),
                    'country': country,
                    'nowPlaying': station.get('nowPlaying') or '-',
                    'description': description,
                    'playable': station.get('playable'),
                    'streamUrl': station.get('streamUrl')
                }

                Radio_API.debug("Normalized station data to: {0}".format(normalizedStation))
                normalizedStations.append(normalizedStation)

            return normalizedStations

    @staticmethod
    def __getStationPropertyValue(property):
        ''' Helper method

        Some endpoints returns e.g {"name": "some name"}
        and others {"name": {"matchHighlights": [], "value": "some name"}}
        '''
        try:
            return property.get('value') if property else ''
        except:
            return property

    @staticmethod
    def __addStreamUrlToStation(station):
        ''' Helper method

        Select a stream url that is valid.
        has no real logic now but should be extended with perferred bitrate and so on
        '''
        for stream in station.get('streamUrls'):
            if stream.get('streamStatus') == "VALID":
                station['streamUrl'] = stream.get('streamUrl')

            if stream.get('metaDataAvailable'):
                break

        return station

    @staticmethod
    def log(text):
        pyotherside.send('log', text)

    @staticmethod
    def debug(text):
        pyotherside.send('debug', text)

    @staticmethod
    def notify_error(text):
        pyotherside.send('notifyError', text)

radio = Radio_API();
