module Fastlane
  module Actions
    module SharedValues
      SFDK_RENAME_ORIGINAL_NAME = :SFDK_RENAME_ORIGINAL_NAME,
      SFDK_RENAME_NEW_NAME = :SFDK_RENAME_NEW_NAME
    end

    class SfdkRenameAction < Action
      def self.run(params)
        FastlaneCore::PrintTable.print_values(config: params, title: "Summary for SFDK Build")
        old_name = params[:application_name]
        new_name = params[:new_application_name]

        Dir.glob("**/" + old_name + "*.*").each do |entry|
          newEntry = entry.gsub(old_name, new_name)
          File.rename(entry, newEntry)
          puts "Rename from " + entry + " to " + newEntry
        end
 
        Dir.glob(File.join("**", "*.{desktop,pro,spec,yaml,qml,cpp,h}")).each do |file|
          if File.open(file).read() =~ /#{old_name}/
            puts "update " + file
            text = File.open(file).read()
            replace = text.gsub!(/#{old_name}/, new_name)
            File.open(file, "w") { |f| f.puts replace }
          end
        end

        Actions.lane_context[SharedValues::SFDK_RENAME_ORIGINAL_NAME] = old_name
        Actions.lane_context[SharedValues::SFDK_RENAME_NEW_NAME] = new_name
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "Rename a sailfish application"
      end

      def self.details
        "Rename"
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :application_name,
            env_name: 'FL_SFDK_RENAME_NAME',
            description: 'The current application name',
            type: String,
            optional: false),
          FastlaneCore::ConfigItem.new(key: :new_application_name,
            env_name: 'FL_SFDK_RENAME_NEW_NAME',
            description: 'The location of where to put rpm',
            type: String,
            optional: false)
        ]
      end

      def self.output
        [
          ['SFDK_RENAME_ORIGINAL_NAME', 'The original application name before rename'],
          ['SFDK_RENAME_NEW_NAME', 'The new application name after rename']
        ]
      end

      def self.example_code
        [
          'sfdk_rename(
            application_name: "original-name",
            new_application_name: "new-name"
          )'
        ]
      end

      def self.authors
        ["Kempe"]
      end

      def self.is_supported?(platform)
        platform == :sailfish
      end
    end
  end
end
