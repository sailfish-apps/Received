import QtQuick 2.6
import Sailfish.Silica 1.0


PullDownMenu {
    id: pulleyMeny

    property alias actionSettings: actionSettings
    property alias actionShowPlayer: actionShowPlayer
    property alias actionBrowse: actionBrowse
    property alias actionSearch: actionSearch

    property bool hideSettingsAction: false
    property bool hideShowPlayerAction: false
    property bool hideBrowseAction: false
    property bool hideSearchAction: false

    MenuItem {
        id: actionSettings
        visible: !hideSettingsAction
        text: qsTr("Settings")
    }
    MenuItem {
        id: actionShowPlayer
        visible: isPlaying && !player.open && !hideShowPlayerAction
        text: qsTr("Show player")
    }
    MenuItem {
        id: actionBrowse
        visible: !hideBrowseAction
        text: qsTr("Browse")
    }
    MenuItem {
        id: actionSearch
        visible: !hideSearchAction
        text: qsTr("Search")
    }
}
