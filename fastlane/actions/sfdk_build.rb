require 'fileutils'

module Fastlane
  module Actions
    module SharedValues
      SFDK_BUILD_OUTPUT_DIRECTORY = :SFDK_BUILD_OUTPUT_DIRECTORY
    end

    class SfdkBuildAction < Action
      def self.run(params)
        FastlaneCore::PrintTable.print_values(config: params, title: "Summary for SFDK Build")
        tooling = params[:tooling]
        architectures = params[:arch]
        output_directory = File.expand_path(params[:output_directory])
        root_dir = Dir.pwd

        UI.user_error!("sfdk is not installed or on $PATH") if `which sfdk`.length == 0
        # I can not get sfdk to install other toolings. So for now...
        UI.user_error!("Tooling #{tooling} is not installed. use maintain-tools to install it first") unless tooling_installed?(tooling)

        UI.message "Creating build with tooling: #{tooling}"

        Dir.mkdir(output_directory) unless File.exist?(output_directory)
        tmp_dir = Dir.mktmpdir("build-", root_dir)
        architectures.each do |arch|
          sh "sfdk", "config", "target=#{tooling}-#{arch}"
          UI.message "Building for: #{arch}"

          build_dir = File.join(tmp_dir, arch)
          Dir.mkdir(build_dir)
          Dir.chdir(build_dir) do
            sh "sfdk", "build", root_dir
            artifacts = Dir.glob("RPMS/*.rpm")
            FileUtils.mv(File.join(artifacts), output_directory)
          end
        end

        FileUtils.rm_rf tmp_dir unless params[:keep_build_directroy]
        
        Actions.lane_context[SharedValues::SFDK_BUILD_OUTPUT_DIRECTORY] = output_directory
      end

      def self.tooling_installed?(tooling)
        system "sfdk tools list | grep #{tooling}"
        $?.exitstatus == 0
      end

      #####################################################
      # @!group Documentation
      #####################################################

      def self.description
        "A short description with <= 80 characters of what this action does"
      end

      def self.details
        "You can use this action to do cool things..."
      end

      def self.available_options
        [
          FastlaneCore::ConfigItem.new(key: :keep_build_directroy,
                                        description: "Set this to true if you want to save all files produced by the build",
                                        is_string: false,
                                        optional: true,
                                        default_value: false),
          FastlaneCore::ConfigItem.new(key: :output_directory,
                                        env_name: 'FL_SFDK_OUTPUT_DIRECTORY',
                                        description: 'The location of where to put rpm',
                                        type: String,
                                        default_value: 'output'),
          FastlaneCore::ConfigItem.new(key: :arch,
                                        env_name: "FL_SFDK_ARCH",
                                        description: "Specify which architecture to build [all, i486, armv7hl]",
                                        type: Array,
                                        default_value: ["i486", "armv7hl"],
                                        optional: true,
                                        verify_block: proc do |values|
                                          allowed = %w(i486 armv7hl)
                                          UI.user_error!("The platform can only be #{types}") unless (values - allowed).empty?
                                        end),
          FastlaneCore::ConfigItem.new(key: :tooling,
                                        env_name: "FL_SFDK_TOOLING",
                                        description: "Specify which tooling to use. eg: SailfishOS-3.3.0.16",
                                        type: String,
                                        optional: false)
        ]
      end

      def self.output
        [
          ['SFDK_BUILD_OUTPUT_DIRECTORY', 'The path to the output directory']
        ]
      end

      def self.example_code
        [
          'sfdk_build(
            tooling: "SailfishOS-3.3.0.16"
          )',
          'sfdk_build(
            tooling: "SailfishOS-3.3.0.16",
            arch: ["i486", "armv7hl"],
            output_directory: "archives"
          )'
        ]
      end

      def self.authors
        ["kempe"]
      end

      def self.is_supported?(platform)
        platform == :sailfish
      end

      def self.category
        :building
      end
    end
  end
end
