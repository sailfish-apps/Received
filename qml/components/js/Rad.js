.pragma library

function getStationFromRadioJson(jsonData) {
    return {
        radIoId: jsonData.id,
        name: jsonData.name,
        stationLogo: jsonData.thumbnail,
        country: jsonData.country,
        genre: jsonData.genres,
        url: jsonData.streamUrl,
        description: jsonData.description
    };
}
