VERSION=$1

sed -i "s/Version:.*/Version:    $VERSION/g" rpm/harbour-received.spec
sed -i "s/Version:.*/Version: $VERSION/g" rpm/harbour-received.yaml
