import QtQuick 2.6
import Sailfish.Silica 1.0

import "../components/listmodels/"

ListItem {
    contentHeight: column.height + Theme.paddingLarge

    Column {
        id: column

        anchors {
            left: parent.left
            leftMargin: Theme.horizontalPageMargin
            right: parent.right
            rightMargin: Theme.horizontalPageMargin
            verticalCenter: parent.verticalCenter
        }
        
        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            color: Theme.highlightColor
            font.pixelSize: Theme.fontSizeLarge
            text: language
        }
        
        Repeater {
            model: translators
            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeExtraSmall
                text: name
            }
        }
    }
}
