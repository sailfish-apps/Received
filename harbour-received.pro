# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-received

QT += multimedia dbus
CONFIG += sailfishapp

# Write version file
VERSION_H = \
"$${LITERAL_HASH}ifndef RECEIVED_VERSION" \
"$${LITERAL_HASH}   define RECEIVED_VERSION \"$$VERSION\"" \
"$${LITERAL_HASH}   define RECEIVED_BUILD_NUMBER \"$$BUILD_NUMBER\"" \
"$${LITERAL_HASH}endif"
write_file($$$$OUT_PWD/received_version.h, VERSION_H)

HEADERS += \
    src/audioplayer.h \
    src/mprisbase.h \
    src/mprisplayer.h

SOURCES += \
    src/harbour-received.cpp \
    src/audioplayer.cpp \
    src/mprisbase.cpp \
    src/mprisplayer.cpp

DISTFILES += \
    qml/components/Settings.qml \
    qml/components/audioplayer/DockedAudioPlayerForm.ui.qml \
    qml/components/audioplayer/DockedAudioPlayer.qml \
    qml/components/audioplayer/PlayerControlsForm.ui.qml \
    qml/components/contextmenus/FavoritesListContextMenu.qml \
    qml/components/contextmenus/StationsListContextMenu.qml \
    qml/components/CategoryDelegate.ui.qml \
    qml/components/CustomSearchField.qml \
    qml/components/MenuLabelSmal.ui.qml \
    qml/components/NavigationMenuForm.ui.qml \
    qml/components/NavigationMenu.qml \
    qml/components/RadioAPI.qml \
    qml/components/RecivedUpgrader.qml \
    qml/components/StationDelegate.qml \
    qml/cover/CoverPage.qml \
    qml/pages/BrowsePageForm.ui.qml \
    qml/pages/BrowsePage.qml \
    qml/pages/BrowseByCategoryPageForm.ui.qml \
    qml/pages/BrowseByCategoryPage.qml \
    qml/pages/FavoritesPageForm.ui.qml \
    qml/pages/FavoritesPage.qml \
    qml/pages/FavoritePageForm.ui.qml \
    qml/pages/FavoritePage.qml \
    qml/pages/SettingsPageForm.ui.qml \
    qml/pages/SettingsPage.qml \
    qml/pages/StationsPageForm.ui.qml \
    qml/pages/StationsPage.qml \
    qml/harbour-received.qml \
    qml/cover/layouts/CoverDesign.qml \
    qml/components/audioplayer/StationInfoLabel.qml \
    qml/components/listmodels/ApiLanguageListModel.qml \
    qml/components/audioplayer/SleepTimerSwitch.qml \
    qml/components/sleeptimer/SleepTimerDialogForm.ui.qml \
    qml/components/sleeptimer/SleepTimerDialog.qml \
    qml/components/sleeptimer/SleepTimer.qml \
    qml/components/listmodels/PlayerLayoutListModel.qml \
    qml/components/audioplayer/DockedAudioPlayerLoader.qml \
    qml/components/audioplayer/DockedAudioPlayerSmall.qml \
    qml/components/audioplayer/DockedAudioPlayerSmallForm.ui.qml \
    qml/pages/AboutPageForm.ui.qml \
    qml/pages/AboutPage.qml \
    qml/components/listmodels/TranslationCreditsListModel.qml \
    qml/pages/TranslationCreditsPage.qml \
    qml/pages/TranslationCreditsPageForm.qml

OTHER_FILES += \
    rpm/harbour-received.changes.in \
    rpm/harbour-received.changes.run.in \
    rpm/harbour-received.spec \
    rpm/harbour-received.yaml \
    translations/*.ts \
    qml/components/js/utils/log.js \
    qml/components/js/Utils.js \
    qml/components/js/Storage.js \
    qml/components/js/Favorites.js \
    qml/components/js/Rad.js \
    qml/components/python/api.py \
    harbour-received.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

CONFIG += sailfishapp_i18n

TRANSLATIONS += translations/harbour-received-es.ts \
                translations/harbour-received-fr.ts \
                translations/harbour-received-ru.ts \
                translations/harbour-received-sv.ts
