import QtQuick 2.6
import Sailfish.Silica 1.0

Page {
    id: aboutPage

    property alias translationAction: translationAction
    property alias issueAction: issueAction
    property alias sourceAction: sourceAction

    SilicaFlickable {
        id: aboutPageFlickable
        anchors.fill: parent
        contentHeight: pageColumns.height
        VerticalScrollDecorator { flickable: aboutPageFlickable }

        Column {
            id: pageColumns
            width: parent.width
            spacing: Theme.paddingMedium

            PageHeader {
                title: qsTr("About Received")
            }

            Image {
                anchors.horizontalCenter: parent.horizontalCenter
                fillMode: Image.PreserveAspectFit
                source: "/usr/share/icons/hicolor/256x256/apps/harbour-received.png"
            }


            Label {
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                wrapMode: Text.WordWrap
                maximumLineCount: 2
                horizontalAlignment: Text.AlignHCenter
                text: "Received"
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeExtraSmall
                color: Theme.secondaryColor
                text: qsTr("Version: ") + Qt.application.version
            }

            ButtonLayout {
                width: parent.width

                Button {
                    id: translationAction
                    text: qsTr("Translations")
                    ButtonLayout.newLine: true
                }

                Button {
                    id: issueAction
                    text: qsTr("Report an Issue")
                    ButtonLayout.newLine: true
                }

                Button {
                    id: sourceAction
                    text: qsTr("Source code")
                    ButtonLayout.newLine: true
                }
            }
        }
    }
}
