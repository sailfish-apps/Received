import QtQuick 2.0
import Nemo.Configuration 1.0

ConfigurationGroup {
    path: "/apps/harbour-received/settings"

    property string apiUrl: "http://api.rad.io"
    property string playerLayout: "DockedAudioPlayerSmall"
    property int favoritSchemaVersion: 0
}
