import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"


Page {
    id: page

    property alias browseByListView: browseByListView
    property alias browseByModel: browseByModel
    property alias navigationMenu: navigationMenu

    property string category: ""
    property string headerTitle: ""

    SilicaListView {
        id: browseByListView
        VerticalScrollDecorator { flickable: browseByListView }
        anchors.fill: parent

        NavigationMenu {
            id: navigationMenu
            hideBrowseAction: true
        }

        ViewPlaceholder {
             enabled: !app.loading && browseByListView.count === 0
             text: qsTr("No data returned")
         }

        header: Column {
                    PageHeader {
                        title: qsTr("Browse by") + " " + headerTitle
                        width: page.width
                    }
                }

        currentIndex: -1
        model: ListModel {
            id: browseByModel
        }

        delegate: CategoryDelegate {
            id: categoryItem

            Connections {
                target: categoryItem
                onClicked: browseByListView.currentIndex = index
            }
        }
    }
}
