import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Notifications 1.0

import it.kempe.AudioPlayer 1.0
import "pages"
import "components"
import "components/audioplayer" as Audio
import "./components/js/Storage.js" as DB
import "./components/js/Utils.js" as Utils

ApplicationWindow
{
    id: app

    property alias notification: notification
    property alias settings: settings
    property bool loading
    property var stationData

    property bool isPlaying: AudioPlayer.isPlaying

    allowedOrientations: Orientation.All
    bottomMargin: player.visibleSize
    initialPage: favorites
    cover: Qt.resolvedUrl("cover/CoverPage.qml")

    RecivedUpgrader {
    }

    Settings {
        id: settings
    }

    Notification {
        id: notification
        isTransient: true
     }

    FavoritesPage {
        id: favorites
    }

    Audio.DockedAudioPlayerLoader {
        id: player
    }

    PageBusyIndicator {
        id: spinner
        running: loading
    }

    RadioAPI {
        id: radioAPI
    }

    function playerLayoutUpdated() {
        player.updateDockedAudioPlayerSource();
    }
}


