#include "mprisbase.h"
#include <QDebug>

MprisBase::MprisBase(QObject *parent) : QDBusAbstractAdaptor(parent)
{
    m_dekstopEntry = "harbour-received";
    m_identity = "Received";
}

QStringList MprisBase::SupportedUriSchemes()
{
    return QStringList() << "http" << "https" << "rtsp";
}

QStringList MprisBase::SupportedMimeTypes()
{
    return QStringList() << "audio/mpeg" << "audio/mpeg3" << "audio/wav";
}

QString MprisBase::DesktopEntry()
{
    return m_dekstopEntry;
}

QString MprisBase::Identity()
{
    return m_identity;
}

void MprisBase::setFullScreen(const bool)
{
    qDebug() << "MprisBase: SetPosition not supported";
}

void MprisBase::Raise()
{
    qDebug() << "MprisBase: Raise not supported";
}

void MprisBase::Quit()
{
    qDebug() << "MprisBase: Quit not supported";
}
